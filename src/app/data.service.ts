import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // local
  // private REST_API_SERVER = 'http://localhost:20081/api';
  // Test
  private REST_API_SERVER = 'https://meownekko-studio.ru/api';
  private TOKEN: string | null = '';
  private options = {
    headers: HttpHeaders
  };

  constructor(private httpClient: HttpClient) {
    this.login();
  }

  // tslint:disable-next-line:typedef
  private login() {
    if (!this.TOKEN) {
      this.TOKEN = localStorage.getItem('ACCESS_TOKEN');
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Authorization', 'Bearer ' + this.TOKEN);

      // @ts-ignore
      this.options.headers = headers;
    }
  }

  public sendGetRequest(method: string | undefined): Observable<never[]> {
    if (!this.TOKEN) {
      this.login();
    }

    // @ts-ignore
    return this.httpClient.get<never[]>(this.REST_API_SERVER + method, this.options);
  }

  public sendPostRequest(method: string | undefined, data: any): Observable<any[]> {
    if (!this.TOKEN) {
      this.login();
    }

    // @ts-ignore
    return this.httpClient.post<any[]>(this.REST_API_SERVER + method, data, this.options);
  }

  public sendPutRequest(method: string | undefined, data: any): Observable<any[]> {
    if (!this.TOKEN) {
      this.login();
    }

    // @ts-ignore
    return this.httpClient.put<any[]>(this.REST_API_SERVER + method, data, this.options);
  }

  public sendDeleteRequest(method: string | undefined): Observable<any[]> {
    if (!this.TOKEN) {
      this.login();
    }

    // @ts-ignore
    return this.httpClient.delete<any[]>(this.REST_API_SERVER + method, this.options);
  }
}
