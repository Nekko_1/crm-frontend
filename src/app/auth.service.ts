import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable()
export class AuthService {
  private isLoggedIn: boolean;

  constructor() {
    this.isLoggedIn = false;
  }

  // tslint:disable-next-line:typedef ban-types
  public login(userInfo: User) {
    // @ts-ignore
    localStorage.setItem('ACCESS_TOKEN', userInfo.token);
    localStorage.setItem('ORGANIZATION_ID', userInfo.organization_id);
    this.isLoggedIn = localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  // tslint:disable-next-line:typedef
  logoutUser(): void {
    localStorage.removeItem('ACCESS_TOKEN');
    this.isLoggedIn = false;
  }

  // tslint:disable-next-line:typedef
  isUserLoggedIn(): boolean {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  getOrganizationId(): string {
    // @ts-ignore
    return localStorage.getItem('ORGANIZATION_ID') !== null;
  }
}
