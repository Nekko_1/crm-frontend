import { Component, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../../auth.service';
import {DataService} from '../../data.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent {
  @Output()
  toggleSidebar = new EventEmitter<void>();
  data = [];

  public showSearch = false;

  constructor(
    private dataService: DataService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.sendGetRequest('/user/current').subscribe((data: never[]) => {
      this.data = Object.values(data) ? Object.values(data)[0] : [];

      console.log(this.data);
    });
  }

  getUserImages() {
    // @ts-ignore
    return this.data['images'] ? this.data['images'] : false;
  }

  logout() {
    this.authService.logoutUser();
  }
}
