import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})

export class FileUploadService {

  // API url
  baseApiUrl = 'https://file.io';

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
  ) { }

  // Returns an observable
  // @ts-ignore
  upload(file) {

    return new Observable<any>((observer) => {
      // Create form data
      const formData = new FormData();

      // Store form name as "file" with file data
      // @ts-ignore
      formData.append('file', file, file['name']);

      // Make http post request over api
      // with formData as req
      // tslint:disable-next-line:no-shadowed-variable
      this.dataService.sendPostRequest('/import', formData).subscribe((data: any[]) => {
        observer.next(data);
      });
    });
  }
}
