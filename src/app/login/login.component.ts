import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router} from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  // @ts-ignore
  loginForm: FormGroup;
  isSubmitted  =  false;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm  =  this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    if (this.authService.isUserLoggedIn()) {
      this.router.navigate(['home']).then(r => console.log(r));
    }
  }

  // tslint:disable-next-line:typedef
  get formControls() {
    // @ts-ignore
    return this.loginForm.controls;
  }

  // tslint:disable-next-line:typedef
  login() {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.loginForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    // @ts-ignore
    this.dataService.sendPostRequest('/login', this.loginForm.value).subscribe((data: any[]) => {
      // @ts-ignore
      if (data.token) {
        // @ts-ignore
        this.loginForm.value.token = data.token;
        // @ts-ignore
        this.loginForm.value.organization_id = data.organization_id;
        this.authService.login(this.loginForm.value);
        this.router.navigate(['dashboard']).then(r => console.log(r));
      }
    });
  }
}
