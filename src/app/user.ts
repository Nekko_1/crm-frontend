export interface User {
  email: string;
  password: string;
  token: string;
  organization_id: string;
}
