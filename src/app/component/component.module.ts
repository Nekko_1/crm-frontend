// @ts-ignore
import { NgModule } from '@angular/core';
// @ts-ignore
import { RouterModule } from '@angular/router';
// @ts-ignore
import { CommonModule } from '@angular/common';
// @ts-ignore
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// @ts-ignore
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ComponentsRoutes } from './component.routing';
import { NgbdpregressbarBasicComponent } from './progressbar/progressbar.component';
import { NgbdpaginationBasicComponent } from './pagination/pagination.component';
import { NgbdAccordionBasicComponent } from './accordion/accordion.component';
import { NgbdAlertBasicComponent } from './alert/alert.component';
import { NgbdCarouselBasicComponent } from './carousel/carousel.component';
import { NgbdDatepickerBasicComponent } from './datepicker/datepicker.component';
import { NgbdDropdownBasicComponent } from './dropdown-collapse/dropdown-collapse.component';
import { NgbdModalBasicComponent } from './modal/modal.component';
import { NgbdPopTooltipComponent } from './popover-tooltip/popover-tooltip.component';
import { NgbdratingBasicComponent } from './rating/rating.component';
// import { NgbTabChangeEvent } from './tabs/tabs.component';
import { NgbdtimepickerBasicComponent } from './timepicker/timepicker.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { CardsComponent } from './card/card.component';
import { DealsComponent } from './deal/deal.component';
import { CarsComponent } from './car/car.component';
import { UsersComponent } from './user/user.component';
import { markModelComponent } from './mark-model/mark-model.component';
import { ToastComponent } from './toast/toast.component';
import { ToastsContainer } from './toast/toast-container';
import { NgSelectModule } from '@ng-select/ng-select';
import { CategoryComponent } from './category/category.component';
import { CategoryItemComponent } from './category/category.item.component';
import { ProductComponent } from './product/product.component';
import { ProfileComponent } from './profile/profile.component';
import { StockComponent } from './stock/stock.component';
import { ImportComponent } from './import/import.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    NgbdpregressbarBasicComponent,
    NgbdpaginationBasicComponent,
    NgbdAccordionBasicComponent,
    NgbdAlertBasicComponent,
    NgbdCarouselBasicComponent,
    NgbdDatepickerBasicComponent,
    NgbdDropdownBasicComponent,
    NgbdModalBasicComponent,
    NgbdPopTooltipComponent,
    NgbdratingBasicComponent,
    // NgbTabChangeEvent,
    NgbdtimepickerBasicComponent,
    ButtonsComponent,
    CardsComponent,
    DealsComponent,
    markModelComponent,
    CarsComponent,
    UsersComponent,
    ToastComponent,
    ToastsContainer,
    CategoryComponent,
    CategoryItemComponent,
    ProductComponent,
    ProfileComponent,
    StockComponent,
    ImportComponent
  ]
})
export class ComponentsModule {}
