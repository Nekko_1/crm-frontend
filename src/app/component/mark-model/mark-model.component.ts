import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mark-model',
  templateUrl: 'mark-model.component.html'
})

// tslint:disable-next-line:class-name
export class markModelComponent implements OnInit {
  data = [];
  componentName = 'mark';
  checkedMark = [];
  checkedModel = [];
  closeResult = '';

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal
  ) {}

  async ngOnInit(): Promise<void> {
    await this.dataService.sendGetRequest('/settings/mark').subscribe((data: never[]) => {
      if (data) {
        // @ts-ignore
        this.checkedMark = Object.values(data)[0].value.split(',');
        console.log(this.checkedMark);
      }
    });

    await this.dataService.sendGetRequest('/settings/model').subscribe((data: never[]) => {
      if (data) {
        // @ts-ignore
        this.checkedModel = Object.values(data)[0].value.split(',');
        console.log(this.checkedModel);
      }
    });

    this.getData();
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName + '/all').subscribe((data: never[]) => {
      this.data = Object.values(data);
      console.log(this.data);
    });
  }

  check(type: string, id: number) {
    // @ts-ignore
    let index;

    if (type === 'mark') {
      // @ts-ignore
      if (this.checkedMark.indexOf(id.toString()) >= 0) {
        // @ts-ignore
        index = this.checkedMark.indexOf(id.toString());

        if (index !== -1) {
          this.checkedMark.splice(index, 1);
        }
      } else {
        // @ts-ignore
        this.checkedMark.push(id.toString());
      }
    } else {
      // @ts-ignore
      if (this.checkedModel.indexOf(id.toString()) >= 0) {
        // @ts-ignore
        index = this.checkedModel.indexOf(id.toString());

        if (index !== -1) {
          this.checkedModel.splice(index, 1);
        }
      } else {
        // @ts-ignore
        this.checkedModel.push(id.toString());
      }
    }
  }

  isCheckedMark(id: number) {
    // @ts-ignore
    return this.checkedMark.indexOf(id.toString()) >= 0;
  }

  isCheckedModel(id: number) {
    // @ts-ignore
    return this.checkedModel.indexOf(id.toString()) >= 0;
  }

  selectAll(models: []) {
    let index;

    models.forEach( (model) => {
      // @ts-ignore
      if (this.checkedModel.indexOf(model['id'].toString()) >= 0) {
        // @ts-ignore
        index = this.checkedModel.indexOf(model['id'].toString());

        if (index !== -1) {
          this.checkedModel.splice(index, 1);
        }
      } else {
        // @ts-ignore
        this.checkedModel.push(model['id'].toString());
      }
    });
  }

  save(type: string | Blob): void {
    const formData = new FormData();
    formData.append('model', type);
    formData.append('value', type === 'mark' ? this.checkedMark.join(',') : this.checkedModel.join(','));

    // @ts-ignore
    this.dataService.sendPostRequest('/settings', formData).subscribe((data: never[]) => {
      // @ts-ignore
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // This is for the Second modal
  open2(content2:string) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
