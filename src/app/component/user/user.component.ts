import {Component, OnInit, TemplateRef} from '@angular/core';
import { DataService } from '../../data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { User } from './user';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html'
})
export class UsersComponent implements OnInit {
  data = [];
  componentName = 'user';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  user: User[] | undefined;
  isSubmitted  =  false;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {
    this.editForm  =  formBuilder.group({
        first_name: [],
        last_name: [],
        phone: [],
        date_of_birth: [],
        images: [],
        city: [],
        email: [],
    });
    this.createForm  =  formBuilder.group({
      first_name: [],
      last_name: [],
      phone: [],
      date_of_birth: [],
      images: [],
      city: [],
      email: [],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName).subscribe((data: never[]) => {
      this.data = Object.values(data);
    });
  }

  // tslint:disable-next-line:typedef
  create(user: User) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, user).subscribe((data: any[]) => {
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // tslint:disable-next-line:typedef
  save(user: User, id: string | undefined) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, user).subscribe((data: any[]) => {
        console.log(data);

        this.modalService.dismissAll();
        this.getData();
      });
    }
  }

  delete(id: string | Blob): void {
    // @ts-ignore
    this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
      console.log(data);

      this.getData();
    });
  }

  // This is for the Second modal
  edit(content1: TemplateRef<any>) {
    this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
