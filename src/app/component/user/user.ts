export interface User {
  id: number;
  first_name: string;
  last_name: string;
  phone: string;
  date_of_birth: string;
  images: string;
  city: string;
  email: string;
}
