import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {DataService} from '../../data.service';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FileUploadService } from '../../file-upload.service';
import {IStock} from '../stock/stock';
import {parseJson} from '@angular/cli/utilities/json-file';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  // Variable to store shortLink from api response
  importData: any[] | undefined;
  message = '';
  progressBar = '';
  // @ts-ignore
  loading = false; // Flag variable
  // @ts-ignore
  file: File = null; // Variable to store file
  data = [];
  componentName = 'import';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  isSubmitted  =  false;
  activeCategory: number | undefined;
  // @ts-ignore
  categories: [];
  activeStock: number | undefined;
  // @ts-ignore
  stocks: IStock[];
  imports = [];

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private fileUploadService: FileUploadService
  ) {
    this.createForm  =  this.formBuilder.group({
      stock: [],
      percentage_allowance: [],
      file: []
    });

    this.editForm  =  this.formBuilder.group({
      stock: [],
      percentage_allowance: [],
      file: []
    });
  }

  ngOnInit(): void {
    this.getStocks();
    this.getImports();
  }

  // tslint:disable-next-line:typedef
  onChangeStock(value: any) {
    this.activeStock = value;
  }

  // On file Select
  onChange(event: Event) {
    // @ts-ignore
    this.createForm.value.file = event.target.files[0];
  }

  getStocks() {
    this.dataService.sendGetRequest('/stock').subscribe((data: never[]) => {
      this.stocks = Object.values(data);
    });
  }

  // tslint:disable-next-line:typedef
  create(form: FormGroup) {
    this.isSubmitted = true;

    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    this.createForm.value.stock = this.activeStock;

    const formData = new FormData();
    formData.append('file', this.createForm.value.file, this.createForm.value.file['name']);
    formData.append('stock', this.createForm.value.stock);
    formData.append('percentage_allowance', this.createForm.value.percentage_allowance ?? 0);

    this.repeatImport(formData);
  }

  // tslint:disable-next-line:typedef
  repeatImport(formData: any) {
    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, formData).subscribe((data: any[]) => {
      // @ts-ignore
      if (data['status'] === 'complete') {
        // @ts-ignore
        this.message = data['message'];
        this.loading = false;
      } else {
        this.loading = true;

        // @ts-ignore
        data['data'] = JSON.parse(data['data']);

        setInterval(() => {
          // @ts-ignore
          this.getStatusImport(data['data']['id']);
        }, 10000);
      }
    }, err => {
      console.log(err.message);
    }, () => {
      console.log('completed');
    });
  }

  // tslint:disable-next-line:typedef
  getImports() {
    this.dataService.sendGetRequest('/schedule').subscribe((data: any[]) => {
      // @ts-ignore
      this.imports = Object.values(data);

      this.imports.forEach((item, index) => {
        // @ts-ignore
        item['message'] = JSON.parse(item['message']);
        // @ts-ignore
        if (item['message']['success_count'] && item['message']['count']) {
          // @ts-ignore
          item['message']['progressBar'] = item['message']['success_count'] / item['message']['count'] * 100;
          // @ts-ignore
          item['message']['progressBar'] = item['message']['progressBar'].toFixed(2);
        } else {
          // @ts-ignore
          item['message']['progressBar'] = 0;
        }
      });
    });
  }

  // tslint:disable-next-line:typedef
  getStatusImport(id: string) {
    this.dataService.sendGetRequest('/schedule/' + id).subscribe((data: any[]) => {
      // @ts-ignore
      this.importData = data;
      // @ts-ignore
      this.importData['data'] = JSON.parse(this.importData['data']);
      // @ts-ignore
      this.importData['data']['message'] = JSON.parse(this.importData['data']['message']);
      // @ts-ignore
      this.progressBar = this.importData['data']['message']['success_count'] / this.importData['data']['message']['count'] * 100;
    });
  }
  // tslint:disable-next-line:typedef
  // save(form: FormGroup, id: string | undefined) {
  //   this.isSubmitted = true;
  //
  //   if (this.activeCategory) {
  //     this.editForm.value.category_id = this.activeCategory;
  //   }
  //   // @ts-ignore
  //   if (this.editForm.invalid) {
  //     return;
  //   }
  //
  //   // tslint:disable-next-line:no-shadowed-variable
  //   if (id !== 'undefined') {
  //     this.dataService.sendPutRequest('/' + this.componentName + '/' + id, this.editForm.value).subscribe((data: any[]) => {
  //       console.log(data);
  //
  //       this.modalService.dismissAll();
  //       this.getData();
  //     });
  //   }
  // }
  //
  // delete(id: string | Blob): void {
  //   // @ts-ignore
  //   this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
  //     console.log(data);
  //
  //     this.getData();
  //   });
  // }
  //
  // // This is for the Second modal
  // edit(content1: TemplateRef<any>) {
  //   this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;
  //   }, (reason) => {
  //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //   });
  // }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
