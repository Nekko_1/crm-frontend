export interface Car {
  id: number;
  vin_number: string;
  gos_number: string;
  user_id: number;
  images: string;
  buy_date: string;
  model: string;
  mark: string;
  description: string;
}
