import {Component, OnInit, TemplateRef} from '@angular/core';
import { DataService } from '../../data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { IMark } from './mark';
import { IModel } from './model';

@Component({
  selector: 'app-car',
  templateUrl: 'car.component.html'
})
export class CarsComponent implements OnInit {
  data = [];
  componentName = 'car';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  isSubmitted  =  false;
  // @ts-ignore
  marks: IMark[];
  // @ts-ignore
  models: IModel[];
  activeModel: number | undefined;
  activeMark: number | undefined;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {
    this.createForm  =  formBuilder.group({
      vin_number: [],
      gos_number: [],
      user_id: [],
      images: [],
      buy_date: [],
      model: [],
      mark: [],
      description: [],
    });

    this.editForm  =  formBuilder.group({
      vin_number: [],
      gos_number: [],
      user_id: [],
      images: [],
      buy_date: [],
      model: [],
      mark: [],
      description: [],
    });
  }

  ngOnInit(): void {
    this.getData();
    this.getMarks();
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName).subscribe((data: never[]) => {
      this.data = Object.values(data);
    });
  }

  // tslint:disable-next-line:typedef
  create(form: FormGroup) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, this.createForm.value).subscribe((data: any[]) => {
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // tslint:disable-next-line:typedef
  save(form: NgForm, id: string | undefined) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, this.editForm.value).subscribe((data: any[]) => {
        console.log(data);

        this.modalService.dismissAll();
        this.getData();
      });
    }
  }

  delete(id: string | Blob): void {
    // @ts-ignore
    this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
      console.log(data);

      this.getData();
    });
  }

  // This is for the Second modal
  edit(content1: TemplateRef<any>) {
    this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  // tslint:disable-next-line:typedef
  getModels() {
    // @ts-ignore
    this.dataService.sendGetRequest('/model').subscribe((data: never[]) => {
      this.models = Object.values(data);

      // console.log('model');
      // console.log(this.models);
    });
  }

  // tslint:disable-next-line:typedef
  getMarks() {
    // @ts-ignore
    this.dataService.sendGetRequest('/mark').subscribe((data: never[]) => {
      this.marks = Object.values(data);

      // @ts-ignore
      this.models = Object.values(this.marks)[0].models;

      // @ts-ignore
      this.activeMark = Object.values(this.marks)[0].id;
      // @ts-ignore
      this.activeModel = Object.values(this.models)[0].id;

      // console.log('mark');
      // console.log(this.marks);
    });
  }

  // tslint:disable-next-line:typedef
  onChangeModel(value: any) {
    this.activeModel = value;
  }

  // tslint:disable-next-line:typedef
  onChangeMark(value: any) {
    this.activeMark = value;

    this.marks.forEach((item, index) => {
      // @ts-ignore
      if ((this.marks[index].id).toString().indexOf(value) >= 0) {
        // @ts-ignore
        this.models = this.marks[index].models;
        // @ts-ignore
        this.activeModel = Object.values(this.models).length ? Object.values(this.models)[0].id : undefined;
      }
    });

    // console.log('value: ' + value);
  }
}
