import {Component, OnInit, TemplateRef} from '@angular/core';
import { DataService } from '../../data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-deal',
  templateUrl: 'deal.component.html'
})
export class DealsComponent implements OnInit {
  data = [];
  componentName = 'order';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  isSubmitted  =  false;
  users: [] | undefined;
  cars: [] | undefined;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.getData();

    this.createForm  =  this.formBuilder.group({
      description: ['', Validators.required],
      address: ['', Validators.required],
      status: ['', Validators.required],
      price: ['', Validators.required],
      user: [''],
      user_id: [''],
      car: [''],
      car_id: ['']
    });

    this.editForm  =  this.formBuilder.group({
      description: ['', Validators.required],
      address: ['', Validators.required],
      status: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName).subscribe((data: never[]) => {
      this.data = Object.values(data);
    });
  }

  search(searchName: string) {
    // tslint:disable-next-line:no-shadowed-variable
    // @ts-ignore
    this.dataService.sendGetRequest('/' + searchName + '/search/' + this.createForm.value[searchName]).subscribe((data: any[]) => {
      // @ts-ignore
      this[searchName + 's'] = data;
      console.log(data);
    });
  }

  changeUser(id: any) {
    // @ts-ignore
    const result = this.users.filter(function(el){
      // tslint:disable-next-line:radix
      return el['id'] === parseInt(id);
    });

    if (result.length) {
      if (result[0]['cars']['id']) {
        this.cars = [];
        this.cars.push(result[0]['cars']);
      } else {
        this.cars = result[0]['cars'];
      }
    }
  }

  changeCar(id: any) {
    // @ts-ignore
    const result = this.cars.filter(function(el){
      // tslint:disable-next-line:radix
      return el['id'] === parseInt(id);
    });

    if (result.length) {
      if (result[0]['users']['id']) {
        this.users = [];
        this.users.push(result[0]['users']);
      } else {
        this.users = result[0]['users'];
      }
    }
  }

  // tslint:disable-next-line:typedef
  create(form: FormGroup) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, this.createForm.value).subscribe((data: any[]) => {
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // tslint:disable-next-line:typedef
  save(form: NgForm, id: string | undefined) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, this.editForm.value).subscribe((data: any[]) => {
        console.log(data);

        this.modalService.dismissAll();
        this.getData();
      });
    }
  }

  delete(id: string | Blob): void {
    // @ts-ignore
    this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
      console.log(data);

      this.getData();
    });
  }

  // This is for the Second modal
  edit(content1: TemplateRef<any>) {
    this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
