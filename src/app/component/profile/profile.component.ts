import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {User} from '../user/user';
import {DataService} from '../../data.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  data = [];
  componentName = 'user';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  user: User[] | undefined;
  isSubmitted  =  false;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
  ) {
    this.editForm  =  formBuilder.group({
      first_name: [],
      last_name: [],
      phone: [],
      date_of_birth: [],
      images: [],
      city: [],
      email: [],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName + '/current').subscribe((data: never[]) => {
      this.data = Object.values(data);

      console.log(this.data);
    });
  }

  // tslint:disable-next-line:typedef
  save(user: User, id: string | undefined) {
    this.isSubmitted = true;
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, user).subscribe((data: any[]) => {
        console.log(data);

        this.getData();
      });
    }
  }
}
