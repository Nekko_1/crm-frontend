import { Component } from '@angular/core';
import {NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-ngbd-tabs',
  templateUrl: './tabs.component.html'
})

// tslint:disable-next-line:component-class-suffix
export class NgbTabChangeEvent {
  currentJustify = 'start';

  currentOrientation = 'horizontal';
  public beforeChange($event: NgbTabChangeEvent) {
    // @ts-ignore
    if ($event.nextId === 'tab-preventchange2') {
      $event.preventDefault();
    }
  }

  private preventDefault() {

  }
}
