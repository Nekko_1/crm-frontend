import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {DataService} from '../../data.service';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  data = [];
  componentName = 'stock';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  isSubmitted  =  false;
  activeCategory: number | undefined;
  // @ts-ignore
  categories: [];

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {
    this.createForm  =  this.formBuilder.group({
      article: [],
      name: [],
      manufacturer: [],
      city: [],
      delivery_date: [],
      description: []
    });

    this.editForm  =  this.formBuilder.group({
      article: [],
      name: [],
      manufacturer: [],
      city: [],
      delivery_date: [],
      description: []
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName).subscribe((data: never[]) => {
      console.log(data);
      this.data = Object.values(data);
    });
  }

  // tslint:disable-next-line:typedef
  create(form: FormGroup) {
    this.isSubmitted = true;

    if (this.activeCategory) {
      this.createForm.value.category_id = this.activeCategory;
    }
    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, this.createForm.value).subscribe((data: any[]) => {
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // tslint:disable-next-line:typedef
  save(form: FormGroup, id: string | undefined) {
    this.isSubmitted = true;

    if (this.activeCategory) {
      this.editForm.value.category_id = this.activeCategory;
    }
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, this.editForm.value).subscribe((data: any[]) => {
        console.log(data);

        this.modalService.dismissAll();
        this.getData();
      });
    }
  }

  delete(id: string | Blob): void {
    // @ts-ignore
    this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
      console.log(data);

      this.getData();
    });
  }

  // This is for the Second modal
  edit(content1: TemplateRef<any>) {
    this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
