export interface IStock {
    article: string,
    name: string,
    manufacturer: string,
    city: string,
    delivery_date: string,
    description: string
}
