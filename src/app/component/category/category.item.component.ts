import {Input, Output, EventEmitter, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-category-item',
  templateUrl: './category.item.component.html'
})
export class CategoryItemComponent implements OnInit {
  @Input() categories: any;
  // @ts-ignore
  @Output() category = new EventEmitter<any>();

  public show = false;
  public categoryActive: string | undefined;

  ngOnInit(): void {
  }

  toggle() {
    this.show = !this.show;
  }

  changeCategoryContent(category: any) {
    this.category.emit(category);
  }
}
