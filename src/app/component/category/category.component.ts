import {Component, OnInit, TemplateRef} from '@angular/core';
import { DataService } from '../../data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  data = [];
  componentName = 'category';
  closeResult = '';
  // @ts-ignore
  createForm: FormGroup;
  // @ts-ignore
  editForm: FormGroup;
  isSubmitted  =  false;
  activeCategory: number | undefined;
  // @ts-ignore
  selectedCategories: 1;
  categories: [] | undefined;
  category: [] | undefined;

  constructor(
    private dataService: DataService,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.getData();

    this.createForm  =  this.formBuilder.group({
      status: [false],
      is_active_site: [false],
      name: [],
      description: [],
      images: [],
      parent_id: []
    });

    this.editForm  =  this.formBuilder.group({
      status: [false],
      is_active_site: [false],
      name: [],
      description: [],
      images: [],
      parent_id: []
    });
  }

  getData() {
    this.dataService.sendGetRequest('/' + this.componentName).subscribe((data: never[]) => {
      // @ts-ignore
      this.data = Object.values(data['data']);
    });
  }

  // tslint:disable-next-line:typedef
  create(form: FormGroup) {
    this.isSubmitted = true;

    if (this.activeCategory) {
      this.createForm.value.parent_id = this.activeCategory;
    }
    // @ts-ignore
    if (this.createForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    this.dataService.sendPostRequest('/' + this.componentName, this.createForm.value).subscribe((data: any[]) => {
      console.log(data);

      this.modalService.dismissAll();
      this.getData();
    });
  }

  // tslint:disable-next-line:typedef
  save(form: FormGroup, id: string | undefined) {
    this.isSubmitted = true;

    if (this.activeCategory) {
      this.editForm.value.parent_id = this.activeCategory;
    }
    // @ts-ignore
    if (this.editForm.invalid) {
      return;
    }

    // tslint:disable-next-line:no-shadowed-variable
    if (id !== 'undefined') {
      this.dataService.sendPutRequest('/' + this.componentName + '/' + id, this.editForm.value).subscribe((data: any[]) => {
        console.log(data);

        this.modalService.dismissAll();
        this.getData();
      });
    }
  }

  delete(id: string | Blob): void {
    // @ts-ignore
    this.dataService.sendDeleteRequest('/' + this.componentName + '/' + id).subscribe((data: never[]) => {
      console.log(data);

      this.getData();
    });
  }

  // This is for the Second modal
  edit(content1: TemplateRef<any>) {
    this.modalService.open(content1, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  createPopup(content2: TemplateRef<any>) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  check(form: FormGroup, type: string, id: number) {
    let result;

    if (id) {
      // @ts-ignore
      this.data.forEach( (category) => {
        // @ts-ignore
        if (category['id'] === id) {
          // @ts-ignore
          category[type] = Number(!category[type]);
          result = !!category[type];
        }
      });

      form.controls[type].setValue(result);
    } else {
      form.controls[type].setValue(!form.controls[type].value);
    }

    return form.controls[type].value;
  }

  isChecked(form: FormGroup, type: string, id: number) {
    let result;

    if (id) {
      // @ts-ignore
      this.data.forEach( (category) => {
        // @ts-ignore
        if (category['id'] === id) {
          result = !!category[type];
        }
      });

      form.controls[type].setValue(result);
    }

    return form.controls[type].value;
  }

  // tslint:disable-next-line:typedef
  onChangeCategory(value: any) {
    this.activeCategory = value;
  }

  changeCategoryData(category: any) {
    this.category = category;
  }
}
